package net.parkermc.coins.items;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.parkermc.coins.CoinsMod;

public class CoinsItems {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, CoinsMod.MODID);

    public static final RegistryObject<CoinItem> coin_10 = ITEMS.register("coin_10", () -> new CoinItem("10"));
    public static final RegistryObject<CoinItem> coin_100 = ITEMS.register("coin_100", () -> new CoinItem("100"));
    public static final RegistryObject<CoinItem> coin_1k = ITEMS.register("coin_1k", () -> new CoinItem("1k"));
    public static final RegistryObject<CoinItem> coin_10k = ITEMS.register("coin_10k", () -> new CoinItem("10k"));
    public static final RegistryObject<CoinItem> coin_1m = ITEMS.register("coin_1m", () -> new CoinItem("1m"));
}
